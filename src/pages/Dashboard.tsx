import { RouteComponentProps } from '@reach/router';
import React from 'react';
import { Breadcrumb, Grid } from 'semantic-ui-react';
import { LatestMessages } from '../components/LatestMessages';
import { LatestQrCodes } from '../components/LatestQrCodes';

export type tDashboardProps = RouteComponentProps & {};

export const Dashboard: React.FC<tDashboardProps> = () => {
  return (
    <div>
      <Grid>
        <Grid.Row>
          <Breadcrumb>
            <Breadcrumb.Section link>Домашня сторінка</Breadcrumb.Section>
            <Breadcrumb.Divider icon='right chevron' />
          </Breadcrumb>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column mobile={16} computer={8}>
            <LatestMessages />
          </Grid.Column>
          <Grid.Column mobile={16} computer={8}>
            <LatestQrCodes />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};
