/* eslint-disable  @typescript-eslint/ban-ts-ignore */
import React from 'react';
import { RouteComponentProps } from '@reach/router';
import {
  Header,
  Segment,
  Image,
  Grid,
  Message,
  Step,
  Icon,
  TextArea,
  Form,
  Button,
  Checkbox,
  Divider,
} from 'semantic-ui-react';
import styled from 'styled-components';
import { UserIcon } from '../components/UserIcon';
import { useStoreActions } from '../store';
import dayjs from 'dayjs';
import { QrCode } from '../components/QrCode';

export type tQrEditorProps = RouteComponentProps & {};

const FullHeight = styled.div`
  min-height: 60vh;
`;

export const Contact: React.FC<tQrEditorProps> = () => {
  const [name, setName] = React.useState('');
  const [message, setMessage] = React.useState('');
  const [saved, setSaved] = React.useState(false);

  const addChat = useStoreActions((state) => state.chat.addChat);

  const save = () => {
    setSaved(true);
    addChat({
      chat: {
        id: `${~~(1000000 * Math.random())}`,
        qr: '1',
        from: name,
        date: dayjs(),
        messages: [
          {
            from: name,
            date: dayjs(),
            id: `${~~(1000000 * Math.random())}`,
            text: message,
            read: false,
          },
        ],
      },
    });
  };

  const download = () => {
    fetch('https://avatars.dicebear.com/api/code/http://localhost:3000/chats/112345891.svg')
      .then((resp) => resp.blob())
      .then((blob) => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');

        a.style.display = 'none';
        a.href = url;
        // the filename you want
        a.download = 'chat.svg';
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
      });
  };

  // @ts-ignore
  return (
    <FullHeight>
      <Segment vertical>
        <Grid centered>
          <Grid.Row>
            <Image
              src='https://cdn.pixabay.com/photo/2016/03/31/17/59/arm-1294052_960_720.png'
              circular
              size='small'
            />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column textAlign='center'>
              <Header as='h2'>Овва, ви знайшли чужу річ! Вітаємо!</Header>
              <Header as='h3'>Ось, що про неї каже власник:</Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={8}>
              <Message>
                <Message.Header>
                  <UserIcon id='Данило Казимиров' /> Привіт, мене звуть Данило
                </Message.Header>
                <p>Я хочу повернути цей телефон, бо його мені подарувала моя бабця.</p>
                Вона казала, що скоріше закінчиться світ, аніж моя Нокія перестане працювати.
                Нажаль, схоже, що я її загубив(
                <p>
                  Я буду дуже вдячний, якщо ти напишеш мені та ми домовимося про його повернення.
                  Сподіваюся, що ти чесна та порядна людина і обов&apos;язково допоможеш мені
                </p>
              </Message>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column textAlign='center'>
              <Header as='h3'>Як ви можете допомогти?</Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={5}>
              <Step.Group size='small' vertical>
                <Step completed>
                  <Icon name='code' />
                  <Step.Content>
                    <Step.Title>Ви не робот?</Step.Title>
                    <Step.Description>
                      Пройдіть перевірку, яка підтвердить <br /> що ви не робот
                    </Step.Description>
                  </Step.Content>
                </Step>

                <Step completed={saved} active={!saved}>
                  <Icon name='envelope outline' />
                  <Step.Content>
                    <Step.Title>Напишіть листа</Step.Title>
                    <Step.Description>
                      Напишіть повідомлення бідоласі, <br />
                      яка щось загубила, обнадійте її
                    </Step.Description>
                  </Step.Content>
                </Step>

                <Step active={saved}>
                  <Icon name='coffee' />
                  <Step.Content>
                    <Step.Title>Зачекайте</Step.Title>
                    <Step.Description>
                      Доки власник <br />
                      речі розпочне чат із вами
                    </Step.Description>
                  </Step.Content>
                </Step>
                <Step disabled>
                  <Icon name='comment alternate outline' />
                  <Step.Content>
                    <Step.Title>Поверніть річ</Step.Title>
                    <Step.Description>І заробіть плюсик до карми</Step.Description>
                  </Step.Content>
                </Step>
              </Step.Group>
            </Grid.Column>
            <Grid.Column textAlign='left' width={8}>
              {!saved && (
                <Form>
                  <Form.Field>
                    <label>Як до вас звертатися?</label>
                    <Form.Input value={name} onChange={(e) => setName(e.target.value)} />
                  </Form.Field>
                  <Form.Field>
                    <label>Що скажете?</label>
                    <TextArea
                      rows={5}
                      // @ts-ignore
                      onChange={(e) => setMessage(e.target.value)}
                      placeholder='Привіт, я знайшов твою річ, якщо хочеш її повернути - зв`яжися зі мною'
                    />
                  </Form.Field>
                  <Form.Field>
                    <Checkbox label='Так, я погоджуюся з умовами користування' />
                  </Form.Field>
                  <Button type='submit' color='green' onClick={save}>
                    Надіслати
                  </Button>
                </Form>
              )}
              {saved && (
                <Message>
                  <Message.Header>
                    Скопіюйте посилання, аби мати змогу відкрити чат пізніше, або збережіть
                    зобреження
                  </Message.Header>
                  <Message.Content>
                    https://povertayko.com/soaoscnalmsclaosakspa-ascpamsclamcam-ascamslcm
                    <Divider />
                    Або завантажте його)
                    <Divider />
                    <Button color='green' onClick={download}>
                      Завантажити
                    </Button>
                    <Divider />
                    <QrCode url='https://povertayko.com/soaoscnalmsclaosakspa-ascpamsclamcam-ascamslcm/' />
                  </Message.Content>
                </Message>
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </FullHeight>
  );
};
