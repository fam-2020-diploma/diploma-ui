import React from 'react';
import { RouteComponentProps } from '@reach/router';
import { Breadcrumb, Grid } from 'semantic-ui-react';
import { QrCodeCreationForm } from '../components/QrCodeCreationForm';
import { QrCodeCreationPreview } from '../components/QrCodeCreationPreview';

export type tQrCreatorProps = RouteComponentProps & {};

export const QrCreator: React.FC<tQrCreatorProps> = () => {
  return (
    <div>
      <Grid>
        <Grid.Row>
          <Breadcrumb>
            <Breadcrumb.Section link>Додому</Breadcrumb.Section>
            <Breadcrumb.Divider icon='right chevron' />
            <Breadcrumb.Section link>мої QR</Breadcrumb.Section>
            <Breadcrumb.Divider icon='right arrow' />
            <Breadcrumb.Section active>створити новий</Breadcrumb.Section>
          </Breadcrumb>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column mobile={16} computer={8}>
            <QrCodeCreationForm />
          </Grid.Column>
          <Grid.Column mobile={16} computer={8}>
            <QrCodeCreationPreview />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};
