/* eslint-disable  @typescript-eslint/ban-ts-ignore */

import React from 'react';
import { RouteComponentProps } from '@reach/router';
import styled from 'styled-components';
import { Form, Button, Grid, Breadcrumb, Divider, Item, Card, Feed } from 'semantic-ui-react';
import { QrCodePreview } from '../components/QrCodePreview';
import { Message } from '../components/Message';
import dayjs from 'dayjs';

export type tChatProps = RouteComponentProps & {};

const ViewArea = styled.div`
  overflow-y: auto;
  max-height: 60vh;
  min-height: 60vh;
`;

const events = [
  {
    from: 'Віктор',
    text: 'Ваш телефончик?',
    images: [
      'https://ixbt.online/live/images/original/00/00/14/2018/03/31/31109dca6a.jpg?w=877',
      'https://img1.freepng.ru/20180604/pgz/kisspng-nokia-3310-nokia-x7-00-nokia-8-nokia-7-nokia-6-nokia-3310-5b14f677d09de2.7346706215281004718545.jpg',
      'https://i2.wp.com/itc.ua/wp-content/uploads/2017/06/0-7.jpg?fit=1500%2C1063&quality=100&strip=all&ssl=1',
    ],
  },
  {
    from: 'Данило Казимиров',
    text: 'Дякую, як мені його повернути?',
  },
  {
    from: 'Віктор',
    text: 'Зателефонуйте мені за номером +380000000007 і я вам допоможу)',
  },
  {
    from: 'Данило Казимиров',
    text: 'А ви точно не шахрай?',
  },
  {
    from: 'Віктор',
    text: 'Точно',
  },
];

export const Chat: React.FC<tChatProps> = () => {
  const [text, setText] = React.useState('');

  const save = () => {
    events.push({
      from: 'Данило Казимиров',
      text,
    });
    setText('');
  };

  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Section link>Додому</Breadcrumb.Section>
        <Breadcrumb.Divider icon='right chevron' />
        <Breadcrumb.Section link>мої чати</Breadcrumb.Section>
        <Breadcrumb.Divider icon='right arrow' />
        <Breadcrumb.Section active>Телефон</Breadcrumb.Section>
      </Breadcrumb>
      <Divider />
      <Grid>
        <Grid.Column width={8} floated='right'>
          <ViewArea>
            <Feed>
              {events.map((e) => (
                <Message message={e} key={1} />
              ))}
            </Feed>
          </ViewArea>
          <Divider />
          <Form reply>
            <Form.TextArea onChange={(e) => setText((e.target as any).value)} value={text} />
            <Button content='Надіслати' labelPosition='left' icon='edit' primary onClick={save} />
          </Form>
        </Grid.Column>
        <Grid.Column width={6}>
          <Card fluid>
            <Card.Content>
              <Item.Group>
                <QrCodePreview
                  qr={{
                    views: 0,
                    id: '1',
                    date: dayjs(),
                    name: 'Телефон',
                    text: 'Поверніть будь-ласка телефон, бо нема з чого дзвонити',
                  }}
                />
              </Item.Group>
            </Card.Content>
          </Card>
          <Divider />
        </Grid.Column>
      </Grid>
    </>
  );
};
