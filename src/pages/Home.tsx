import { RouteComponentProps } from '@reach/router';
import React from 'react';
import styled from 'styled-components';
import Bg from '../images/baggage.jpg';

export type tHomeProps = RouteComponentProps & {};

const HomeDiv = styled.div`
  width: 100vw;
  position: absolute;
  top: 0;
  left: 0;
  height: 100vh;
  background-size: cover;
  background-image: url("${Bg}");
`;

export const Home: React.FC<tHomeProps> = () => {
  return <HomeDiv />;
};
