import React from 'react';
import { redirectTo, RouteComponentProps } from '@reach/router';
import { Breadcrumb, Grid } from 'semantic-ui-react';
import { QrCodeCreationForm } from '../components/QrCodeCreationForm';
import { QrCodeCreationPreview } from '../components/QrCodeCreationPreview';
import { useStoreState } from '../store';

export type tQrCreatorProps = RouteComponentProps<{ id: string }> & {};

export const QrEditor: React.FC<tQrCreatorProps> = ({ id }) => {
  const qr = useStoreState((state) => state.qr.qrs.find((q) => q.id === id));

  if (!qr) {
    redirectTo('/');

    return null;
  }

  return (
    <div>
      <Grid>
        <Grid.Row>
          <Breadcrumb>
            <Breadcrumb.Section link>Додому</Breadcrumb.Section>
            <Breadcrumb.Divider icon='right chevron' />
            <Breadcrumb.Section link>мої QR</Breadcrumb.Section>
            <Breadcrumb.Divider icon='right arrow' />
            <Breadcrumb.Section active>{qr.name}</Breadcrumb.Section>
          </Breadcrumb>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column mobile={16} computer={8}>
            <QrCodeCreationForm qr={qr} />
          </Grid.Column>
          <Grid.Column mobile={16} computer={8}>
            <QrCodeCreationPreview />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};
