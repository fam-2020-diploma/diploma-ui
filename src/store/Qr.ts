import { Action, action, persist } from 'easy-peasy';
import dayjs from 'dayjs';

export type tMessage = {
  id: string;
  text: string;
  from: string;
  date: dayjs.Dayjs;
  read: boolean;
};

export type tQr = {
  id: string;
  views: number;
  date: dayjs.Dayjs;
  name: string;
  text: string;
};

export type tQrState = {
  qrs: tQr[];
  editedQrCode?: tQr | null;
  setEditedQr: Action<tQrState, { qr?: tQr | null }>;
  addQr: Action<tQrState, { qr: tQr }>;
};

export const QrState = persist<tQrState>({
  editedQrCode: null,
  qrs: [
    {
      id: '1',
      views: 1,
      text: 'Заплачу сотку, якщо хтось знайде',
      date: dayjs().add(-6, 'h'),
      name: 'Рюкзак',
    },
    {
      id: '2',
      views: 4,
      date: dayjs().add(Math.random() * 3, 'h'),
      text: 'Повертайте, бо пароль не скажу',
      name: 'Ноут',
    },
    {
      id: '5',
      views: 4,
      date: dayjs().add(-2, 'h'),
      text: 'Не відкривайте! Там конфеденційна інформація!',
      name: 'Сумка',
    },
    {
      id: '3',
      views: 5,
      name: 'Телефон',
      date: dayjs().add(-3, 'h'),
      text: 'Так, я досі користуюся Нокіа, поверніть будь-ласка',
    },
    {
      id: '4',
      name: 'Кросівки',
      views: 1,
      date: dayjs().add(-1, 'h'),
      text: 'Не заберу ніколи, бо вони старі й убиті',
    },
  ],
  addQr: action((state, { qr }) => {
    state.qrs.push(qr);
  }),
  setEditedQr: action((state, { qr }) => {
    state.editedQrCode = qr;
  }),
});
