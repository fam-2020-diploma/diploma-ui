import { createStore, createTypedHooks } from 'easy-peasy';
import { ChatState } from './Chat';
import { QrState } from './Qr';

const initialState = {
  chat: ChatState,
  qr: QrState,
};

export const store = createStore(initialState);
export const { useStoreActions, useStoreState, useStoreDispatch, useStore } = createTypedHooks<
  typeof initialState
>();
