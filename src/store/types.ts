import { tAuthState } from './Auth/types';

export type tAppState = {
  auth: tAuthState;
};
