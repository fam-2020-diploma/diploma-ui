import { action } from 'easy-peasy';
import { tAppState } from '../types';
import { tAuthState } from './types';

const actions = {
  saveAuthData: action<tAppState, { foo: string }>((state: tAppState, payload) => {
    state.auth.foo = payload.foo;
  }),
};

const initialData: tAuthState = {
  foo: '123',
};

export const AuthState = {
  ...actions,
  ...initialData,
};
