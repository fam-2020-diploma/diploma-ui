import { Action, action, persist } from 'easy-peasy';
import dayjs from 'dayjs';

export type tMessage = {
  id: string;
  text: string;
  from: string;
  date: dayjs.Dayjs;
  read: boolean;
};

export type tChat = {
  id: string;
  date: dayjs.Dayjs;
  qr: string;
  messages: tMessage[];
  from: string;
};

export type tChatState = {
  chats: tChat[];
  addChat: Action<tChatState, { chat: tChat }>;
};

export const ChatState = persist<tChatState>({
  chats: [
    {
      id: '112345891',
      qr: '3',
      from: 'Vitya',
      date: dayjs().add(-1, 'd'),
      messages: [
        {
          id: '1',
          text: 'Ваш телефончик?',
          read: false,
          date: dayjs().add(-1, 'd'),
          from: 'Vitya',
        },
      ],
    },
    {
      id: '1123058672',
      qr: '2',
      from: 'Dima',
      date: dayjs().add(-1, 'h'),
      messages: [
        {
          id: '2',
          text: 'Знайшов ноут, здається ваш',
          read: false,
          date: dayjs().add(-1, 'h'),
          from: 'Dima',
        },
      ],
    },
    {
      id: '112305861',
      qr: '5',
      from: 'Lena',
      date: dayjs().add(-1, 'h'),
      messages: [
        {
          id: '1',
          text: 'В сумці були грощі, вже нема',
          read: false,
          date: dayjs().add(-1, 'h'),
          from: 'Lena',
        },
      ],
    },
    {
      id: '112305865',
      qr: '4',
      from: 'Sherlock',
      date: dayjs().add(-1, 'h'),
      messages: [
        {
          id: '1',
          text: 'Гарні найки! поверну за 1000 грн',
          read: false,
          date: dayjs().add(-1, 'h'),
          from: 'Sherlock',
        },
      ],
    },
  ],
  addChat: action((state, { chat }) => {
    state.chats.push(chat);
  }),
});
