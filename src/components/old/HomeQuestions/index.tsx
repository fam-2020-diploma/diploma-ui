// /* eslint-disable react/jsx-props-no-spreading */
// import React from 'react';
// import { Box } from '@chakra-ui/core';
// import css from './style.css';
//
// export type tHomeQuestionsProps = React.ComponentProps<typeof Box> & {};
//
// export const HomeQuestions: React.FC<tHomeQuestionsProps> = ({ children, ...props }) => {
//   return (
//     <Box
//       className={css.question}
//       fontFamily='Press Start 2P'
//       borderWidth={5}
//       borderColor='#293241'
//       rounded={10}
//       w={350}
//       h={200}
//       m={10}
//       p={10}
//       position='relative'
//       {...props}
//     >
//       <Box transform='translate(-50%, -50%)' position='absolute' left='50%' top='50%'>
//         <Box className={css.questionText}>{children}</Box>
//       </Box>
//     </Box>
//   );
// };
