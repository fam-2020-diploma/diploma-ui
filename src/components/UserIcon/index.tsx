import React from 'react';
import { Image } from 'semantic-ui-react';

export const UserIcon: React.FC<
  { id: string; type?: string } & React.ComponentProps<typeof Image>
> = ({ id, type = 'avataaars', ...props }) => {
  return <Image avatar src={`https://avatars.dicebear.com/api/${type}/${id}.svg`} {...props} />;
};
