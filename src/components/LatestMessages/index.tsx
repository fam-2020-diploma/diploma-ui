import { RouteComponentProps } from '@reach/router';
import React from 'react';
import styled from 'styled-components';
import { Card, Icon, Input, Label, Menu, Segment } from 'semantic-ui-react';
import { MessagePreview } from '../MessagePreview';
import { useStoreState } from '../../store';

export type tLatestMessages = RouteComponentProps & {};

const MessagesContainer = styled.div`
  overflow-y: auto;
  height: 60vh;
  margin-top: 10px;
  padding: 5px;
  width: 100%;
`;

export const LatestMessages: React.FC<tLatestMessages> = () => {
  const chats = useStoreState((state) => state.chat.chats);
  const unreadCount = chats.flatMap((chat) => chat.messages).filter((m) => !m.read).length;

  return (
    <Segment>
      <Menu vertical fluid>
        <Menu.Item name='messages'>
          <Label color='teal'>
            <Icon name='mail' /> {unreadCount}
          </Label>
          Непрочитані повідомлення
        </Menu.Item>
        <Menu.Item>
          <Input icon='search' placeholder='Знайти чат за назвою QR' />
        </Menu.Item>
      </Menu>
      <Card.Group>
        <MessagesContainer>
          {chats.map((chat) => (
            <MessagePreview key={chat.id} chat={chat} />
          ))}
        </MessagesContainer>
      </Card.Group>
    </Segment>
  );
};
