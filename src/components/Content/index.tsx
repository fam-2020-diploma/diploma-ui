import styled from 'styled-components';

export const Content = styled.div`
  min-height: calc(100vh - 15em);
  margin-top: 7em;
`;
