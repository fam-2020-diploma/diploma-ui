import React from 'react';
import { Image } from 'semantic-ui-react';

type tProps = React.ComponentProps<typeof Image> & {
  url: string;
};

export const QrCode: React.FC<tProps> = ({ url, ...props }) => {
  const src = `https://avatars.dicebear.com/api/code/${encodeURI(url)}.svg`;

  return <Image src={src} {...props} />;
};
