import React from 'react';
import { Icon, Item, Label } from 'semantic-ui-react';
import { Link } from '@reach/router';
import { useStoreState } from '../store';
import { tQr } from '../store/Qr';
import dayjs from 'dayjs';

type tProps = {
  qr: tQr;
};

export const QrCodePreview: React.FC<tProps> = ({ qr }) => {
  const chatsWithItem = useStoreState((state) =>
    state.chat.chats.filter((chat) => chat.qr === qr.id),
  );
  const unreadMessages = chatsWithItem.flatMap((chat) => chat.messages.filter((m) => !m.read))
    .length;

  return (
    <Item>
      <Item.Image
        size='tiny'
        src={`https://avatars.dicebear.com/api/code/${encodeURI(qr.id)}.svg`}
      />

      <Item.Content>
        <Item.Header as={Link} to={`/qrs/${qr.id}`}>
          {qr.name}
        </Item.Header>
        <Item.Meta>{dayjs(qr.date).format('hh:mm DD/MM/YY')}</Item.Meta>
        <Item.Description>{qr.text || 'Тут мав бути твій опис'}</Item.Description>
        <Item.Extra>
          <Label>
            <Icon name='eye' /> {qr.views}
          </Label>
          <Label>
            <Icon name='envelope' /> {chatsWithItem.length}
          </Label>
          <Label>
            <Icon name='chat' /> {unreadMessages}
          </Label>
        </Item.Extra>
      </Item.Content>
    </Item>
  );
};
