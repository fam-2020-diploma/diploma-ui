import { Link, RouteComponentProps } from '@reach/router';
import React from 'react';
import { Feed, Card } from 'semantic-ui-react';
import { UserIcon } from '../UserIcon';
import { tChat } from '../../store/Chat';
import dayjs from 'dayjs';
import { useStoreState } from '../../store';

export type tLatestMessages = RouteComponentProps & {
  chat: tChat;
};

export const MessagePreview: React.FC<tLatestMessages> = ({ chat }) => {
  const latestMessage = chat.messages[chat.messages.length - 1];
  const qr = useStoreState((state) => state.qr.qrs.find((q) => q.id === chat.qr));

  if (latestMessage.from === 'Danylo Kazymyrov') {
    // todo
    return null;
  }
  const text = `${latestMessage.text.substr(0, 150)}${
    latestMessage.text.length > 150 ? '\n...' : ''
  }`;

  return (
    <Card fluid>
      <Card.Content>
        <Feed>
          <Feed.Event
            as={Link}
            to={`/chats/${chat.id}`}
            image={<UserIcon id={latestMessage.from} />}
            content={`Про: ${qr?.name ?? 'Вашу річ'}`}
            date={dayjs(latestMessage.date).format('hh:mm')}
            summary={latestMessage.from}
            extraText={text}
            meta='прочитати | заблокувати'
          />
        </Feed>
      </Card.Content>
    </Card>
  );
};
