import React from 'react';
import { RouteComponentProps } from '@reach/router';
import { Feed, Icon } from 'semantic-ui-react';
import { UserIcon } from './UserIcon';
import styled from 'styled-components';

export type tQrCreatorProps = RouteComponentProps & {
  message: {
    from: string;
    text: string;
    images?: string[];
  };
};

const CroppedImage = styled.span<{ height: number; width: number; url: string }>`
  width: ${(props) => props.width}px;
  height: ${(props) => props.height}px;
  display: inline-block;
  background-image: url("${(props) => props.url}");
  background-size: cover;
  background-position: center;
  margin: 3px;
  shadow: 1px 10px black;
`;

export const Message: React.FC<tQrCreatorProps> = ({ message }) => {
  return (
    <Feed.Event>
      <Feed.Label>
        <UserIcon id={message.from} />
      </Feed.Label>
      <Feed.Content>
        <Feed.Summary>
          <b>{message.from}</b>
        </Feed.Summary>
        {!!message.images && (
          <Feed.Extra images>
            {message.images.map((url) => (
              <CroppedImage url={url} width={100} key={url} height={100} />
            ))}
          </Feed.Extra>
        )}
        <Feed.Extra text>{message.text}</Feed.Extra>
        <Feed.Meta>
          <Feed.Like>
            <Icon name='eye' color='grey' />
          </Feed.Like>
        </Feed.Meta>
      </Feed.Content>
    </Feed.Event>
  );
};
