import React from 'react';
import { Container, Menu } from 'semantic-ui-react';
import { UserIcon } from '../UserIcon';
import { Link } from '@reach/router';

export type tHeaderProps = {
  activeItem?: string;
};

export const Header: React.FC<tHeaderProps> = () => {
  return (
    <Menu fixed='top' inverted>
      <Container>
        <Menu.Item as={Link} header to='/'>
          <UserIcon id='Повертайко' type='initials' />
          вертайко
        </Menu.Item>
        <Menu.Item as={Link} to='/chats'>
          Мої чати
        </Menu.Item>
        <Menu.Item as={Link} to='/qrs'>
          Мої QR
        </Menu.Item>
        <Menu.Item as={Link} to='/qr/create'>
          Створити QR
        </Menu.Item>
        <Menu.Menu position='right'>
          <Menu.Item>
            Данило
            <UserIcon id='Данило Казимиров' />
          </Menu.Item>
          <Menu.Item as='a'>Вийти</Menu.Item>
        </Menu.Menu>
      </Container>
    </Menu>
  );
};
