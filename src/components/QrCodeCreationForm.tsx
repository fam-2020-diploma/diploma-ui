/* eslint-disable @typescript-eslint/ban-ts-ignore */
import React from 'react';
import { Button, Card, Checkbox, Form, Input, Message, Select, TextArea } from 'semantic-ui-react';
import { tQr } from '../store/Qr';
import { useStoreState, useStoreActions } from '../store';
import dayjs from 'dayjs';

type tProps = {
  qr?: tQr;
};

export const QrCodeCreationForm: React.FC<tProps> = ({ qr }) => {
  const editedQrCode = useStoreState((state) => state.qr.editedQrCode);
  const setEditedQrCode = useStoreActions((state) => state.qr.setEditedQr);
  const addQr = useStoreActions((state) => state.qr.addQr);

  React.useEffect(() => {
    if (qr) {
      setEditedQrCode({ qr });
    } else {
      setEditedQrCode({
        qr: {
          views: 100,
          name: 'Новий QR',
          date: dayjs(),
          id: '0',
          text: '',
        },
      });
    }

    return () => {
      setEditedQrCode({ qr: null });
    };
  }, []);

  const setQrName = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (editedQrCode) {
      editedQrCode.name = event.target.value;
      setEditedQrCode({ qr: editedQrCode });
    }
  };

  const setQrText = (event: React.FormEvent<HTMLTextAreaElement>) => {
    if (editedQrCode) {
      // @ts-ignore
      editedQrCode.text = event.target.value;
      setEditedQrCode({ qr: editedQrCode });
    }
  };

  const save = () => {
    editedQrCode!.id = `${~~(Math.random() * 10000000)}`;
    editedQrCode!.views = 1;
    editedQrCode!.date = dayjs();

    addQr({ qr: editedQrCode! });
  };

  return (
    <>
      <Card fluid>
        <Card.Content>
          <Card.Header>Створіть новий персональний QR-код</Card.Header>
        </Card.Content>
        <Card.Content>
          <Form>
            <Form.Field>
              <label>Як назвемо?</label>
              <Input
                size='big'
                placeholder='Може "Телефончик?'
                value={editedQrCode?.name}
                label={{ icon: 'asterisk' }}
                labelPosition='left corner'
                onChange={setQrName}
              />
            </Form.Field>
            <Form.Field>
              <label>Як розфарбуємо?</label>
              <Select size='big' placeholder='Обирай, бо буде чорний! ^^' options={[]} />
            </Form.Field>
            <Form.Field>
              <label>Що розповісти тому, хто його знайде?</label>
              <TextArea
                size='big'
                placeholder='Привіт, мене звуть Данило, це моя річ, і я дуже хочу її повернути. Будь-ласка, повідом мене про те, що ти її знайшов.'
                rows={10}
                onInput={setQrText}
                value={editedQrCode?.text}
              />
            </Form.Field>

            <Form.Field>
              <Checkbox label='Так, я погоджуюся з умовами користування' />
            </Form.Field>
            <Button type='submit' color='green' onClick={save}>
              {qr ? 'Створити' : 'Зберегти'}
            </Button>
          </Form>
        </Card.Content>
      </Card>
      {!qr && <Message success header='QR-код створено!' content='Не забудьте зберегти його' />}
    </>
  );
};
