import React from 'react';
import { Item, Icon, Input, Label, Menu, Segment } from 'semantic-ui-react';
import styled from 'styled-components';

import { QrCodePreview } from '../QrCodePreview';
import { useStoreState } from '../../store';

export type tLatestQrCodes = {};

const QrsContainer = styled.div`
  overflow-y: auto;
  height: 60vh;
  width: 100%;
  margin-top: 10px;
  padding: 5px;
`;

export const LatestQrCodes: React.FC<tLatestQrCodes> = () => {
  const [filter, setFilter] = React.useState('');
  const qrs = useStoreState((state) =>
    state.qr.qrs.filter((qr) => qr.name.match(new RegExp(filter, 'gi'))),
  );

  return (
    <Segment>
      <Menu vertical fluid>
        <Menu.Item name='messages'>
          <Label color='blue'>
            <Icon name='barcode' /> {qrs.length}
          </Label>
          Ваші QR-коди
        </Menu.Item>
        <Menu.Item>
          <Input
            icon='search'
            placeholder='Знайти QR код за назвою'
            onChange={(e) => setFilter(e.target.value)}
          />
        </Menu.Item>
      </Menu>
      <QrsContainer>
        <Item.Group divided>
          {qrs.map((qr) => (
            <QrCodePreview qr={qr} key={qr.id} />
          ))}
        </Item.Group>
      </QrsContainer>
    </Segment>
  );
};
