import React from 'react';
import { Container, Segment, List } from 'semantic-ui-react';

export type tFooterProps = {};
export const Footer: React.FC<tFooterProps> = () => {
  return (
    <Segment inverted vertical style={{ margin: '5em 0em 0em', padding: '5em 0em' }}>
      <Container textAlign='center'>
        <List horizontal inverted divided link size='small'>
          <List.Item as='a' href='#'>
            Карта сайту
          </List.Item>
          <List.Item as='a' href='#'>
            Зв&apos;язатися із нами
          </List.Item>
          <List.Item as='a' href='#'>
            Умови користування
          </List.Item>
          <List.Item as='a' href='#'>
            Політика конфеденційності
          </List.Item>
        </List>
      </Container>
    </Segment>
  );
};
