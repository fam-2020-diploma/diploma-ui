import React from 'react';
import { RouteComponentProps } from '@reach/router';
import { Card, Item } from 'semantic-ui-react';
import { QrCodePreview } from './QrCodePreview';
import dayjs from 'dayjs';
import { useStoreState } from '../store';

export type tQrCreatorProps = RouteComponentProps & {};

export const QrCodeCreationPreview: React.FC<tQrCreatorProps> = () => {
  const qr = useStoreState((state) => state.qr.editedQrCode);

  return (
    <Card fluid>
      <Card.Content>
        <Card.Header>Як він буде виглядати</Card.Header>
      </Card.Content>
      <Card.Content>
        <Item.Group>
          <QrCodePreview
            qr={
              qr || {
                id: '1',
                name: 'qrcode',
                text: 'Отута буде твій текст',
                date: dayjs(),
                views: 0,
              }
            }
          />
        </Item.Group>
      </Card.Content>
    </Card>
  );
};
