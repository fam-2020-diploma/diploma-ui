import React from 'react';
import { Router } from '@reach/router';
import { Dashboard } from './pages/Dashboard';
import { Home } from './pages/Home';
import { QrCreator } from './pages/QrCreator';
import { Content } from './components/Content';
import { Container } from 'semantic-ui-react';
import { Footer } from './components/Footer';
import { Header } from './components/Header';
import { Chat } from './pages/Chat';
import { Contact } from './pages/Contact';
import { QrEditor } from './pages/QrEditor';

export type tRoutesProps = {
  isAuthenticated: boolean;
};

export const Routes: React.FC<tRoutesProps> = ({ isAuthenticated }) => {
  return (
    <>
      {!window.location.href.startsWith('http://localhost:3000/contact') && <Header />}
      <Content>
        <Container>
          <Router>
            <QrCreator path='/qr/create' />
            <QrEditor path='/qrs/:id' />
            {!isAuthenticated && <Home path='/' default />}
            {isAuthenticated && <Dashboard path='/' default />}
            {isAuthenticated && <Chat path='/chats/:id' />}
            <Contact path='/contact/:id' />
          </Router>
        </Container>
      </Content>
      <Footer />
    </>
  );
};
