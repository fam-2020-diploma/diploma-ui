import React from 'react';
import { StoreProvider } from 'easy-peasy';
import { store } from './store';
import { Routes } from './routes';

export const App: React.FC = () => (
  <StoreProvider store={store}>
    <Routes isAuthenticated />
  </StoreProvider>
);
